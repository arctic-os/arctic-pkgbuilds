cd x86_64
basedir=$(pwd)

for x in *
do
    cd "$basedir/$x"
    echo "Cleaning: $x"
    find . -type f -not -name PKGBUILD -not -name "*.install" -delete
    find . -type d -name "src" -exec rm -rf {} +
    find . -type d -name "pkg" -exec rm -rf {} +
    find . -type d -name "$x" -exec rm -rf {} +
done