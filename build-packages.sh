#!/usr/bin/env bash

pkglist=$(ls x86_64)
basedir=$(pwd)

for x in $pkglist
do
    cd "$basedir/x86_64/$x"
    echo "### Building package $x ###"
    makepkg -cf || echo "Error building package: $x"
done